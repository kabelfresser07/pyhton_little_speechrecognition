import speech_recognition
import pyttsx3

#Audio to text init
speechEngine = speech_recognition.Recognizer()

#Text to audio init
textEngine = pyttsx3.init()

#Main logic
def choice(choice, lang, langRaw):
    if choice == "mic" :
        print("\nWrite the duration in seconds:")
        duration = input("Duration: ")
        print(from_microphone(duration, lang))
    elif choice == "file":
        print("\nWrite the relative path of the file like: Directory/file.wav")
        file_path = input("Path: ")
        from_file(file_path, lang)
    elif choice == "text":
        print("\nWrite the text you want to convert to speech\n")
        text = input("Text: ")
        from_text(text, langRaw)
    else:
        print("\nChoice was not valid, programm is closing...")

#Take audio from File
def from_file(speech_data_file, lang):
    with speech_recognition.AudioFile(speech_data_file) as speechFile:
        data = speechEngine.record(speechFile)
        text_output = speechEngine.recognize_google(data, language=lang)
        print(text_output)

#Take audio from Microphone
def from_microphone(record_duration, lang):
    with speech_recognition.Microphone() as mic:
        print("\nRecording...")
        audio = speechEngine.record(mic, duration=float(record_duration))
        print("Recognition...\n")
        text = speechEngine.recognize_google(audio, language=lang)
        return text

#Take text to audio
def from_text(text, langRaw):
    #Set props
    textEngine.setProperty(lang, langRaw)
    textEngine.setProperty('rate', 140)

    print("\nProcessing text...")
    #Convert text to speech
    textEngine.say(text)

    print("\nPlaying audio...")
    #Output audio generation
    textEngine.runAndWait()
    print("\nAudio completet...")

#Set Language
def set_language(language):
    if language == "de" :
        print("\nSpeech language set to DE - German")
        return "de-DE"
    elif language == "en" :
        print("\nSpeech language set to EN - English")
        return "en-US"
    else:
        print("\nInvalid input..\nSetting DE - German as defaut\n")
        return "de-DE"

#Welcome text and language
print("-------------------------------------------------")
print("\nHello my master, choose your speech language\n\nType 'de' or 'en'")
print("-------------------------------------------------")
inputLang = input("Language: ")

#Sets the language
lang = set_language(inputLang)

#Tutorial
print("-------------------------------------------------")
print("\nFor an audio file type:               file\n")
print("For direct microphone recording type: mic\n")
print("For text input an speech output type: text\n")
print("-------------------------------------------------")

#Take your choice
choiceInput = input("Your choice: ")

#Processing choice
choice(choiceInput, lang, inputLang)

print("\nProgramm is closing...")